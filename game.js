var conteneurSlide = document.querySelector('.conteneur-slide');
class Game {
    //    Défini la class Game
    constructor(background) {
        $('.conteneur-slide').css({
            'background-image': 'url(' + background + ')'
        });

        this.controls();

    }

    controls() {
        //    Faire bouger Mario
        var BARRE_ESPACE = 32, TOP = 38, LEFT = 37, RIGHT = 39;
        
        $(document).keydown(function (e) {
            var marioWidth = 40;
            var boardWidth = 512;
            var mario = $('.mario');
            var position = mario.position();
            var animation = true;
            switch (e.keyCode) {
                case TOP:
                if(animation = true){
                    $('.mario').animate({
                        "top": "-=50px"
                    });
                    $('.mario').animate({
                        "top": "+=50px"
                    });
                    animation = false;
                }
                    break;
                case LEFT:
                    var left = position.left - 50;
                    left = Math.max(left, 0);
                    mario.css({ 'left': left + 'px' });
                    break;
                case RIGHT:
                    var left = position.left + 50;
                    left = Math.min(left, boardWidth - marioWidth);
                    mario.css({ 'left': left + 'px' });
                    break;
            }
        });

    }
}

var step = -10;
// refresh
function refresh() {
    var newPosition = conteneurSlide.offsetLeft  + step;
    if (newPosition < -(3000 - 512)) { return }
    conteneurSlide.style.left = newPosition + 'px';
    if (checkColision()) alert("T'es aussi nul que celui qui à fait ce jeu !!!");
  }
  
  // interval
  setInterval(refresh, 100)


//  CheckCollision
function checkColision() {
    var boum = false;
    $('.mur').each(function () {
        if (colision($('.mario'), $(this))) {
            boum = true;
            return;
        }
    });
    return boum;
}

var marioleft;
//    Collision
function colision(elt1, elt2) {
    
    var p_elt1 = elt1.offset();
    var p_elt2 = elt2.offset();

    if (p_elt1.left < p_elt2.left + elt2.width() &&
        p_elt1.left + elt1.width() > p_elt2.left &&
        p_elt1.top < p_elt2.top + elt2.height() &&
        elt1.height() + p_elt1.top > p_elt2.top) {
        return true;
    }
}

var positiontexture = 0;
function firstBlock () {
    var firstcase;
    for(i=0; i<5; i++){
        $('.conteneur-slide').prepend("<div class='texture_bottom mur'></div>");
        positiontexture = positiontexture + 120;
        $('.texture_bottom:first').css({
            left : positiontexture + 600
        })
    }
}

var positiontexture = 0;
function secondBlock () {
    var firstcase;
    for(i=0; i<5; i++){
        $('.conteneur-slide').prepend("<div class='texture_bottom mur'></div>");
        positiontexture = positiontexture + 120;
        $('.texture_bottom:first').css({
            left : positiontexture + 600,
            bottom : 50
        })
    }
}
